const {ObjectID} = require('mongodb');
const {Todo} = require('../../models/todo');
const jwt = require('jsonwebtoken');
const {Users} = require('../../models/user');

const todos = [{
  _id: new ObjectID(),
  text: 'First test todo',
}, {
  _id: new ObjectID(),
  text: 'Second test todo',
  completedAt: 333,
  completed: true,
}];
userOneID = new ObjectID();
userTwoID = new ObjectID();
const users = [{
  _id: userOneID,
  email: 'user1@email.com',
  password: 'UserOnePass',
  tokens: [{
    access: 'auth',
    token: jwt.sign({_id: userOneID, access: 'auth'}, 'abc123').toString(),
  }],
}, {
  _id: userTwoID,
  email: 'user2@email.com',
  password: 'UserTwoPass',
}];

let populateUsers = (done) => {
  Users.remove({}).then(() => {
    let userOne = new Users(users[0]).save();
    let userTwo = new Users(users[1]).save();
    Promise.all([userOne, userTwo]);
    done();
  });
};

let populateTodos = (done) => {
  Todo.remove({}).then(() => {
    Todo.insertMany(todos);
    done();
  });
};

module.exports = {todos, populateTodos, users, populateUsers};
