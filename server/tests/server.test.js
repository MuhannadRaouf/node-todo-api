const expect = require('expect');
const request = require('supertest');
const {ObjectID} = require('mongodb');

const {app} = require('../server');
const {Todo} = require('../models/todo');
const {Users} = require('../models/user');
const {todos, populateTodos, users, populateUsers} = require('./seed/seed');

beforeEach(populateTodos);
beforeEach(populateUsers);

describe('POST /todos', () => {
  it('Should create a new todo', (done) => {
    let text = 'Test todo text';
    request(app)
        .post('/todos')
        .send({
          text,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.text).toBe(text);
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          Todo.find({
            text,
          }).then((todos) => {
            expect(todos.length).toBe(1);
            expect(todos[0].text).toBe(text);
            done();
          }).catch((err) => done(err));
        });
  });

  it('Should not create todo with invalid body data', (done) => {
    request(app)
        .post('/todos')
        .expect(400)
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Todo.find().then((todos) => {
            expect(todos.length).toBe(2);
            done();
          }).catch((err) => done(err));
        });
  });
});

describe('GET /todos', () => {
  it('Should get all todos', (done) => {
    request(app)
        .get('/todos')
        .expect(200)
        .expect((res) => {
          expect(res.body.docs.length).toBe(2);
        })
        .end(done);
  });
});

describe('GET /todo/:id', () => {
  it('Should return todo doc', (done) => {
    request(app)
        .get(`/todos/${todos[0]._id.toHexString()}`)
        .expect(200)
        .expect((res) => {
          expect(res.body.todo.text).toBe(todos[0].text);
        })
        .end(done);
  });

  it('SHould return 404 if todo not found', (done) => {
    let hexID = new ObjectID().toHexString();
    request(app)
        .get(`/todos/${hexID}`)
        .expect(404)
        .end(done);
  });

  it('Should return 404 for non-object ids', (done) => {
    request(app)
        .get(`/todos/${todos[0]._id.toHexString()}1`)
        .expect(404)
        .end(done);
  });
});

describe('DELETE /todos/:id', () => {
  it('Should Delete the todo doc', (done) => {
    let hexID = todos[0]._id.toHexString();
    request(app)
        .delete(`/todos/${hexID}`)
        .expect(200)
        .expect((res) => {
          expect(res.body.todo.text).toBe(todos[0].text);
        })
        .end((err, res) => {
          if (err) {
            return done(err);
          }

          Todo.findById(hexID).then((res) => {
            expect(res).toNotExist();
            done();
          }).catch((err) => done(err));
        });
  });

  it('Should return 404 if todo is not found', (done) => {
    let id = new ObjectID().toHexString();
    request(app)
        .delete(`/todos/${id}`)
        .expect(404)
        .end(done);
  });

  it('Should return 404 for non-object ids', (done) => {
    request(app)
        .delete(`/todos/${todos[0]._id.toHexString()}1`)
        .expect(404)
        .end(done);
  });
});

describe('PATCH /todos/:id', () => {
  it('Should update the todo', (done) => {
    let id = todos[0]._id.toHexString();
    let text = 'This should be new text';
    request(app)
        .patch(`/todos/${id}`)
        .send({
          completed: true,
          text,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.todo.text).toBe(text);
          expect(res.body.todo.completed).toBe(true);
          expect(res.body.todo.completedAt).toBeA('number');
        })
        .end(done);
  });

  it('Should clear completedAt when to do is not completed', (done) => {
    let id = todos[1]._id.toHexString();
    let text = 'This should be new text';
    request(app)
        .patch(`/todos/${id}`)
        .send({
          completed: false,
          text,
        })
        .expect(200)
        .expect((res) => {
          expect(res.body.todo.text).toBe(text);
          expect(res.body.todo.completed).toBe(false);
          expect(res.body.todo.completedAt).toNotExist();
        })
        .end(done);
  });
});

describe('GET /users/me', () => {
  it('Should get user if authenticated', (done) => {
    request(app)
        .get('/users/me')
        .set('x-auth', users[0].tokens[0].token)
        .expect(200)
        .expect((res) => {
          expect(res.body._id).toBe(users[0]._id.toHexString());
          expect(res.body.email).toBe(users[0].email);
        })
        .end(done);
  });
  it('Should return 401 when user not authenticated', (done) => {
    request(app)
        .get('/users/me')
        .expect(401)
        .expect((res) => {
          expect(res.body).toEqual({});
        })
        .end(done);
  });
});

describe('POST /users', () => {
  it('Should create a user', (done) => {
    let email = 'test@test.com';
    let password = 'testPass';
    request(app)
        .post('/users')
        .send({email, password})
        .expect(200)
        .expect((res) => {
          expect(res.header['x-auth']).toExist();
          expect(res.body._id).toExist();
          expect(res.body.email).toExist();
        })
        .end((err) => {
          if (err) {
            return done(err);
          }
          Users.findOne({email}).then((user) => {
            expect(user).toExist();
            expect(user.password).toNotBe(users.password);
            done();
          });
        });
  });
  it('Should not create user with invalid data', (done) => {
    invalidTestUser = {
      email: 'test@test.com',
      password: 'test',
    };
    request(app)
        .post('/users')
        .send(invalidTestUser)
        .expect(400)
        .end(done);
  });
  it('Should not create user if email is exist', (done) => {
    request(app)
        .post('/users')
        .send(users[0])
        .expect(400)
        .end(done);
  });
});

